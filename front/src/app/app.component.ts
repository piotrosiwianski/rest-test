import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  template: `
    <button (click)="callRest()">Call</button>
    <span>{{value}}</span>
  `
})
export class AppComponent {
  title = 'rest-test';
  value = 'nope';

  constructor(private httpClient: HttpClient) {}

  callRest() {
    console.log('call')
    this.httpClient.get<Response>('http://localhost:8080/').subscribe(response => {
      this.value = response.value;
    });
  }

}

class Response {
  value = "";
}
